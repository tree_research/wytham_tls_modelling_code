#Andrew Burt - a.burt.12@ucl.ac.uk

cmake_minimum_required(VERSION 2.8)
project(treeseg)

SET(CMAKE_CXX_FLAGS "-std=c++0x")
find_package(PCL 1.7 REQUIRED)
include_directories(${PCL_INCLUDE_DIRS} /media/STORAGE1/Wytham/wytham_code/treeseg/include)
link_directories(/media/STORAGE1/Wytham/wytham_code/treeseg/lib)

IF(${CMAKE_SYSTEM_NAME} MATCHES "Linux")

	add_executable(rxp2pcd rxp2pcd.cpp)
	target_link_libraries(rxp2pcd pthread scanlib-mt riboost_system-mt)

	add_executable(rxp2pcd_POSnaming rxp2pcd_POSnaming.cpp)
	target_link_libraries(rxp2pcd_POSnaming pthread scanlib-mt riboost_system-mt)

	add_executable(rxp2tree rxp2tree.cpp)
	target_link_libraries(rxp2tree pthread scanlib-mt riboost_system-mt ${PCL_LIBRARIES})

ENDIF()

add_executable(plotcoords plotcoords.cpp)
target_link_libraries(plotcoords)

