# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/media/STORAGE1/Wytham/wytham_code/treeseg/src/mergeclusters_noReclustering.cpp" "/media/STORAGE1/Wytham/wytham_code/treeseg/build/CMakeFiles/mergeclusters_noReclustering.dir/mergeclusters_noReclustering.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DISABLE_PCAP"
  "DISABLE_PNG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/vtk-5.8"
  "/usr/include/pcl-1.7"
  "/usr/include/eigen3"
  "/usr/include/ni"
  "/usr/include/openni2"
  "/media/STORAGE1/Wytham/wytham_code/treeseg/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
